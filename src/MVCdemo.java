/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matt
 */
public class MVCdemo {
    public static void main(String [] args){
        Student model = retrieveFromDataBase(); 
        StudentView view = new StudentView();
        StudentController controller = new StudentController(model,view);
        
        controller.updateView();
        
        controller.setStudentName("John");
        controller.setStudentID("112");
        
        controller.updateView();
        
    }
    
    private static Student retrieveFromDataBase(){
        Student student = new Student("Robert", "10");
        return student;
    }
}
